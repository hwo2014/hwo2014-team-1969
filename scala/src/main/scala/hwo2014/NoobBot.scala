package hwo2014

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import org.json4s.JsonAST.JValue
import org.json4s.JValue
import scala.util.Try
import scala.collection.JavaConverters._
import collection.mutable.Map

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  val MINIMUM_SPEED = 0.3D
  val MAXIMUM_SPEED = 1D
  val MAX = 1D
  implicit val formats = new DefaultFormats{}
  println("Start socket")
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  var pieces: List[Map[String,Any]] = List()

  println("Send join")
//  send(MsgWrapper("join", Join(botName, botKey)))
  send(MsgWrapper("joinRace", JoinFull(Join(botName, botKey))))
  println("Start play")
  play()

  private def _isPieceWithTurn(pos: Int): Boolean =
    _getPos(pos).get("turn").get.asInstanceOf[Boolean] &&
    (_getPos(pos).get("angle").get.asInstanceOf[Double] > 35D ||
    _getPos(pos).get("angle").get.asInstanceOf[Double] < -35D)

  private def _getPos(pos: Int) = if(pos > 0){
    pieces.lift(pos).getOrElse(pieces(pos - pieces.length))
  }else{
    pieces.lift(pos).getOrElse(pieces(pieces.length + pos))
  }

  private def mod(i: Int): Int = if (i < 0) i * -1 else i
  private def mod(i: Double): Double = if (i < 0) i * -1 else i

  private def getPrevCurrentNextAngle(pos: Int): (Double,Double,Double) =
    (_getPos(pos-1).get("maxAngle").getOrElse(0D).asInstanceOf[Double],
     _getPos(pos).get("maxAngle").getOrElse(0D).asInstanceOf[Double],
     _getPos(pos+1).get("maxAngle").getOrElse(0D).asInstanceOf[Double])

  private def getPrevSpeed(pos: Int): Double = _getPos(pos-1).get("maxSpeed").get.asInstanceOf[Double]
  private def updatePrevSpeed(pos: Int,diff: Double) = {
    val prevPos = pos - 1
    val newSpeedWithDiff = _getPos(prevPos).get("maxSpeed").get.asInstanceOf[Double] + diff
    val newSpeed = if(newSpeedWithDiff > MINIMUM_SPEED) newSpeedWithDiff else MINIMUM_SPEED
    if(prevPos > 0){
      pieces.lift(prevPos).getOrElse(pieces(prevPos - pieces.length))("maxSpeed") = newSpeed
    }else{
      pieces.lift(prevPos).getOrElse(pieces(pieces.length + prevPos))("maxSpeed") = newSpeed
    }
  }

  private def nextMaxAngleGoesDown(pos: Int): Boolean =
    _getPos(pos).get("maxAngle").get.asInstanceOf[Double] > _getPos(pos+1).get("maxAngle").get.asInstanceOf[Double] &&
    _getPos(pos).get("maxAngle").get.asInstanceOf[Double] - _getPos(pos+1).get("maxAngle").get.asInstanceOf[Double] > 2D

  private def _turnIn2Ahead(pos: Int): Boolean =
    !_getPos(pos+1).get("turn").get.asInstanceOf[Boolean] &&
    _getPos(pos+2).get("turn").get.asInstanceOf[Boolean] &&
    (_getPos(pos+2).get("angle").get.asInstanceOf[Double] > 35D ||
      _getPos(pos+2).get("angle").get.asInstanceOf[Double] < -35D)

  private def twoBehindIsLine_?(pos: Int): Boolean =
    !_getPos(pos-1).get("turn").get.asInstanceOf[Boolean] &&
    !_getPos(pos-2).get("turn").get.asInstanceOf[Boolean]

  private def previousIsLine_?(pos: Int): Boolean =
    !_getPos(pos-1).get("turn").get.asInstanceOf[Boolean]

  private def couldIncreaseSpeed_?(pos: Int): Boolean =
    _getPos(pos).get("turn").get.asInstanceOf[Boolean] &&
    _getPos(pos).get("angle").get.asInstanceOf[Double] < 35D &&
    _getPos(pos).get("angle").get.asInstanceOf[Double] > -35D &&
    !_getPos(pos+1).get("turn").get.asInstanceOf[Boolean]

  private def needChangeLine_?(pos: Int, currentLine: Int): (Boolean,Int) =
    if(_getPos(pos+1).contains("switch")){
      (
        currentLine != _getPos(pos+1).get("changetoLane").get.asInstanceOf[Integer],
        _getPos(pos+1).get("changetoLane").get.asInstanceOf[Integer]
      )
    }else{
      (false,0)
    }

  private def isSwitcher_?(pos: Int): Boolean = _getPos(pos).contains("switch")

  private def setChangeToLane(
      list: List[Map[String,Any]],resultList:List[Map[String,Any]]
  ): List[Map[String,Any]] = {
    def getAngleSum(l: List[Map[String,Any]]): Double = {
      l.tail.takeWhile(!_.contains("switch")).foldLeft(0D)((r,i)=>r+i.get("angle").get.asInstanceOf[Double])
    }

    if(list.isEmpty){
      resultList
    }else{
      if(list.head.contains("switch")){
        val nextTurns = list.tail.filter(i => i.get("turn").get.asInstanceOf[Boolean] || i.contains("switch") )
                            .takeWhile(!_.contains("switch"))
        val change = if(nextTurns.isEmpty){
          val refreshList = pieces.filter(i => i.get("turn").get.asInstanceOf[Boolean] || i.contains("switch") )
            .takeWhile(!_.contains("switch"))
          val l = if(refreshList.isEmpty){
            pieces.filter(i => i.get("turn").get.asInstanceOf[Boolean] || i.contains("switch") ).tail
              .takeWhile(!_.contains("switch"))
          }else { refreshList }
          getAngleSum(l) > 0D
          match {
            case true  => 1
            case false => 0
          }
        }else{
          getAngleSum(nextTurns) > 0D
          match {
            case true  => 1
            case false => 0
          }
        } 
        setChangeToLane(list.tail,resultList :+ (list.head + (("changetoLane",change)) ))
      }else{
        setChangeToLane(list.tail,resultList :+ list.head)
      }
    }
  }

  private def setNextFullAngle(l: List[Map[String,Any]], res: List[Map[String,Any]]): List[Map[String,Any]] = {
    if(l.isEmpty){
      res
    }else{
      val nextAngle = if(!l.tail.isEmpty && !l.tail.dropWhile(!_.get("turn").get.asInstanceOf[Boolean]).isEmpty){
        l.tail.dropWhile(!_.get("turn").get.asInstanceOf[Boolean]).takeWhile(_.get("turn").get.asInstanceOf[Boolean])
          .foldLeft(0D)((r,i) =>
            r+(i.get("angle").get.asInstanceOf[Double])*100D/(i.get("radius").get.asInstanceOf[BigInt].toInt)
          )
      }else{
        0D
//        pieces.dropWhile(!_.get("turn").get.asInstanceOf[Boolean]).takeWhile(_.get("turn").get.asInstanceOf[Boolean])
//          .foldLeft(0D)((r,i) => r+i.get("angle").get.asInstanceOf[Double])
      }
      setNextFullAngle(l.tail,res :+ (l.head + (("nextSumAngle",nextAngle))))
    }
  }

  @tailrec private def play(stop: Boolean = true,prevSpeed: Double = 0,prevPos: Int = 0,
                            switched: Boolean = false, prevDist: Double = 0D) {
    val line = reader.readLine()
    if (line != null) {
      val (continue,speed,position,changeLine_?,distance) = Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("gameInit", data) => {
          println("Game init")
          pieces = (data \\ "pieces").children.values.toList
            .map(x => Map(x.asInstanceOf[collection.immutable.Map[String,Any]].toSeq: _*)).map(
              i => if (i.contains("radius")) i+(("turn",true)) else i+(("turn",false))
            ).map( _+(("maxSpeed",0D),("maxAngle",0D),("nextSumAngle",0D)) )
          pieces = setChangeToLane(pieces,List())
          pieces = setNextFullAngle(pieces,List())
          println(pieces)
          (true,MAXIMUM_SPEED,0,(false,0),0D)
        }
        case MsgWrapper("carPositions", data) => {
          val myStats = data.children.values.toList.asInstanceOf[List[collection.immutable.Map[String,Any]]].filter(i => {
            i.get("id").get.asInstanceOf[collection.immutable.Map[String,String]].get("name").getOrElse("") == "UKRGron"
          }).head
          val myPosition = myStats.get("piecePosition").get.asInstanceOf[collection.immutable.Map[String,Any]]
            .get("pieceIndex").get.asInstanceOf[BigInt].toInt
          val prevCurrentNextAngle = getPrevCurrentNextAngle(myPosition)
          val myAngle = myStats.get("angle").get.asInstanceOf[Double]
          val myLane = myStats.get("piecePosition").get.asInstanceOf[collection.immutable.Map[String,Any]].get("lane")
            .get.asInstanceOf[collection.immutable.Map[String,Any]].get("endLaneIndex").get.asInstanceOf[BigInt].toInt
          val myDistance = myStats.get("piecePosition").get.asInstanceOf[collection.immutable.Map[String,Any]]
            .get("inPieceDistance").get.asInstanceOf[Double]
          val speed = if(myPosition != prevPos){
            if(_getPos(myPosition-1).get("turn").get.asInstanceOf[Boolean]){
              println("prev pos with angle")
              //TODO: Get last switch and get line where we will be
//              val lastLastif(!pieces.take(myPosition).isEmpty){
//                pieces.take(myPosition).filter(_.contains("switch")).last.get("changetoLane").getOrElse(myLane)
//              }
              myDistance + ((3.14D * _getPos(myPosition-1).get("radius").get.asInstanceOf[BigInt].toInt *
                _getPos(myPosition-1).get("angle").get.asInstanceOf[Double])/180 - prevDist)
            }else{
              println("prev pos line")
              myDistance + (_getPos(myPosition-1).get("length").get.asInstanceOf[Double] - prevDist)
            }
          }else{
            println("prev pos in same fragment")
            myDistance - prevDist
          }
          val nextAngle = pieces(myPosition).get("nextSumAngle").get.asInstanceOf[Double]
          val nextIsTurn_? = _isPieceWithTurn(myPosition+1)
          val currentIsTurn_? = _isPieceWithTurn(myPosition)
          val newSpeedStrategy = if(nextIsTurn_? || currentIsTurn_?){
            if(myAngle > 20D || myAngle < -20D){
              if(mod(nextAngle) > 75D && mod(nextAngle) < 115D){
                0.075D
              }else{
                0.801D
              }
            }else{
              if(couldIncreaseSpeed_?(myPosition)){
                MAXIMUM_SPEED
              }else{
                if(isSwitcher_?(myPosition) && nextIsTurn_? && twoBehindIsLine_?(myPosition)  ){
                  0.2D
                }else if( nextIsTurn_? || mod(myAngle) > 10D ){
                  if(mod(nextAngle) < 40D){
                    MAXIMUM_SPEED
                  }else if(mod(nextAngle) < 50D){
                    0.802D
                  }else
                  if(mod(nextAngle) < 120D){
                    0.75D
                  }else{
                    0.505D
                  }
                }else{
                  MAXIMUM_SPEED
                }
              }
            }
          }else{
            if(_turnIn2Ahead(myPosition)){
              0.6008D
            }else{
              if(mod(myAngle) < 15D){
                MAXIMUM_SPEED
              }else{
                0.8509D
              }
            }
          }
//          val newSpeed = if(mod(prevCurrentNextAngle._3) < 15D && mod(prevCurrentNextAngle._2) < 15D &&
//            mod(prevCurrentNextAngle._3) != 0D && mod(prevCurrentNextAngle._2) != 0D &&
//            mod(prevCurrentNextAngle._3) != MINIMUM_SPEED && mod(prevCurrentNextAngle._2) != MINIMUM_SPEED
//          ){
//            if(newSpeedStrategy + 0.05D < MAXIMUM_SPEED){
//              println("Incr #1 speed in 0.05 from " + newSpeedStrategy.toString )
//              newSpeedStrategy + 0.05D
//            }else{
//              MAXIMUM_SPEED
//            }
//          }else if(mod(prevCurrentNextAngle._3) > 15D && mod(prevCurrentNextAngle._2) > 15D &&
//            mod(prevCurrentNextAngle._3) != MINIMUM_SPEED && mod(prevCurrentNextAngle._2) != MINIMUM_SPEED &&
//            mod(prevCurrentNextAngle._3) != 0D && mod(prevCurrentNextAngle._2) != 0D
//          ){
//            if(newSpeedStrategy == MINIMUM_SPEED && getPrevSpeed(myPosition) != MINIMUM_SPEED){
//              updatePrevSpeed(myPosition,-0.1D)
//              println("Decr #1 speed in 0.1 from " + newSpeedStrategy.toString )
//              newSpeedStrategy
//            }else if(newSpeedStrategy == MINIMUM_SPEED && getPrevSpeed(myPosition-1) == MINIMUM_SPEED &&
//              getPrevSpeed(myPosition-2) != MINIMUM_SPEED){
//              println("Decr #2 speed in 0.1 from " + newSpeedStrategy.toString )
//              updatePrevSpeed(myPosition-2,-0.1D)
//              newSpeedStrategy
//            }else{
//              if(newSpeedStrategy != MINIMUM_SPEED){
//                if(newSpeedStrategy - 0.05D > 0D){
//                  println("Decr #3 speed in 0.05 from " + newSpeedStrategy.toString )
//                  newSpeedStrategy - 0.05D
//                }else{
//                  0D
//                }
//              }else{
//                newSpeedStrategy
//              }
//            }
//          }else if(mod(prevCurrentNextAngle._3) < mod(prevCurrentNextAngle._2)
//            && mod(nextAngle) < 90D && mod(myAngle) < 30D &&
//            mod(prevCurrentNextAngle._3) != 0D && mod(prevCurrentNextAngle._2) != 0D
//          ){
//            if(newSpeedStrategy + 0.05D < MAX){
//              println("Incr #2 speed in 0.05 from " + newSpeedStrategy.toString )
//              newSpeedStrategy + 0.05D
//            }else{
//              newSpeedStrategy
//            }
//          }else{
//            newSpeedStrategy
//          }
          val newSpeed = newSpeedStrategy
          if(mod(pieces.lift(myPosition).getOrElse(Map()).get("maxAngle").get.asInstanceOf[Double]) < mod(myAngle) ||
             pieces.lift(myPosition).getOrElse(Map()).get("maxSpeed").get.asInstanceOf[Double] > newSpeed
          ){
            pieces.lift(myPosition).get("maxAngle") = myAngle
            pieces.lift(myPosition).get("maxSpeed") = newSpeed
          }
          println(speed)
          (true,newSpeed,myPosition,needChangeLine_?(myPosition,myLane),myDistance)
        }
        case MsgWrapper("gameEnd", _) => (false,MINIMUM_SPEED,0,(false,0),0D)
        case MsgWrapper("lapFinished", _) => {
          println("")
          println("!!! Lap finished !!!")
          println(pieces)
          println("")
          (false,MAXIMUM_SPEED,0,(false,0),0D)
        }
        case MsgWrapper("crash", _) => {
          println("")
          println("!!! Crash !!!")
          println(pieces)
          println("")
          (false,MAXIMUM_SPEED,0,(false,0),0D)
        }
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType); (true,MINIMUM_SPEED,0,(false,0),0D)
        case x => println(x); (true,MINIMUM_SPEED,0,(false,0),0D)
      }

      val switch = if(!changeLine_?._1 || (position == prevPos && switched)){
        send(MsgWrapper("throttle", speed)); switched;
      }else{
        println("switch Lane")
        send(MsgWrapper("switchLane", changeLine_?._2 match {
          case 0 => "Left"
          case 1 => "Right"
        })); true;
      }
      play(continue,speed,position,switch,distance)
    }else if(!stop) play(stop)
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Join(name: String, key: String)
//case class JoinFull(botId: Join, trackName: String = "germany", carCount: Int = 1)
case class JoinFull(botId: Join, trackName: String = "keimola", carCount: Int = 1)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
